<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    public function index()
    {
        $todo = Item::all();
        return $todo;
    }
    public function store(Request $request)
    {
        $todo = Item::create([
            'barang'=> $request->barang,
            'harga'=> $request->harga,
            'jumlah'=> $request->jumlah,
        ]);
        return $todo;
    }
    public function edit(Request $request, $id)
    {
        $todo = Item::find($id);
        $todo->update([
            'barang'=> $request->barang,
            'harga'=> $request->harga,
            'jumlah'=> $request->jumlah,
        ]);
        return $todo;
    }
    public function delete($id)
    {
        Item::destroy($id);
        return 'success';
    }



}
