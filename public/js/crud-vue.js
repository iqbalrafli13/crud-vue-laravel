let vo = new Vue({
    el:'#app',
    data:{
        newBarang:'',
        newHarga:'',
        newJumlah:'',
        editBarang:'',
        editHarga:'',
        editJumlah:'',
        id:'',
        index:'',
        items:[],
    },methods:{
        addItem:function (){
            let barangInput = this.newBarang.trim();
            let hargaInput = this.newHarga.trim();
            let jumlahInput = this.newJumlah.trim();

            if (barangInput && hargaInput && jumlahInput) {
                this.$http.post('/api/item/store',
                    {barang:barangInput, harga:hargaInput ,jumlah:jumlahInput}
                ).then(response => {
                    console.log(response.body.id);
                    this.items.push(
                        {id:response.body.id, barang:barangInput, harga:hargaInput ,jumlah:jumlahInput},
                    )
                    this.newBarang='';
                    this.newHarga='';
                    this.newJumlah='';
                });
            }
        },
        removeItem:function (index,item) {
            
            let conf = confirm("yakin?");
            if (conf) {
                this.$http.post(`/api/item/${item.id}/delete`).then(response => {
                    this.items.splice(index,1)
                });
            }

        },

        editItem:function (index,item){
            this.editBarang = item.barang
            this.editHarga = item.harga
            this.editJumlah = item.jumlah
            this.id = item.id

            this.index = index
        },
        submitEditItem:function (index){

            barangInput = this.editBarang;
            hargaInput = this.editHarga;
            jumlahInput = this.editJumlah;
            let conf = confirm("yakin");
            if (conf) {
                if (barangInput && hargaInput && jumlahInput) {
                    this.$http.post(`/api/item/${this.id}/edit`,
                        {barang:barangInput, harga:hargaInput ,jumlah:jumlahInput}
                    ).then(response => {
                        this.items[index].barang = barangInput
                        this.items[index].harga = hargaInput
                        this.items[index].jumlah = jumlahInput
                    });
                }
            }
        },
    },mounted:function () {
        // GET /someUrl
        this.$http.get('/api/item').then(response => {
            this.items = response.body;
        });
    }

});
