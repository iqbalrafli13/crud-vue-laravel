<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
      <h1>Hello, world!</h1>
      <main id="app" class="container">



              <div class="form-group  row">
                  <label class="col-sm-2 col-form-label">Nama Barang</label>
                  <div class="col-sm-10">
                      <input type="text" v-model="newBarang" class="form-control">
                  </div>
              </div>

              <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Harga</label>
                  <div class="col-sm-10">
                      <input type="number" v-model="newHarga" class="form-control">
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Jumlah</label>
                  <div class="col-sm-10">
                      <input type="number" v-model="newJumlah" class="form-control">
                  </div>
              </div>
              <div class="form-group">
                  <button  v-on:click="addItem" class="btn btn-primary float-right">Submit</button>
              </div>

          <table class="table table-hover">
              <thead>
                  <tr>
                      <th scope="col">No</th>
                      <th scope="col">Barang</th>
                      <th scope="col">Harga</th>
                      <th scope="col">Jumlah</th>
                  </tr>
              </thead>
              <tbody>
                  <tr v-for="(item, index) in items">
                      <th scope="row">@{{ index +1}}</th>
                      <td>@{{item.barang}}</td>
                      <td>@{{item.harga}}</td>
                      <td>@{{item.jumlah}}</td>
                      <td>
                          <button v-on:click="editItem(index,item)" data-toggle="modal" data-target="#exampleModal" class="btn btn-warning float-right">Edit</button>
                          <button v-on:click="removeItem(index,item)" class="btn btn-danger float-right mr-1">Hapus</button>
                      </td>
                  </tr>
              </tbody>
          </table>

          <!-- Button trigger modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
              Launch demo modal
          </button>

          <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          <div class="form-group  row">
                              <label class="col-sm-2 col-form-label">Nama Barang</label>
                              <div class="col-sm-10">
                                  <input type="text" v-model="editBarang" :val="items.editBarang" class="form-control">
                              </div>
                          </div>

                          <div class="form-group row">
                              <label class="col-sm-2 col-form-label">Harga</label>
                              <div class="col-sm-10">
                                  <input type="number" v-model="editHarga" :val="items.editHarga" class="form-control">
                              </div>
                          </div>

                          <div class="form-group row">
                              <label class="col-sm-2 col-form-label">Jumlah</label>
                              <div class="col-sm-10">
                                  <input type="number" v-model="editJumlah" :val="items.editJumlah" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button  v-on:click="submitEditItem(index)" class="btn btn-primary float-right" data-dismiss="modal" aria-label="Close">Submit</button>
                      </div>
                  </div>
              </div>
          </div>

      </main>


      <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
      <script src="{{url('/js/crud-vue.js')}}"></script>

      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

      <script type="text/javascript">
      $('#myModal').on('shown.bs.modal', function () {
          $('#myInput').trigger('focus')
      })

      </script>
  </body>
</html>
